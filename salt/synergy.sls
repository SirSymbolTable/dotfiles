include:
  - tools

{% set version = '1.7.4' %}
{% set syn_dir = '/home/colby/Software/synergy' %}
synergy:
  pkg.installed:
    - pkgs:
      - libcurl4-openssl-dev
      - xorg-dev
      - libqt4-dev
      - libavahi-compat-libdnssd-dev
      - libssl-dev
  git.latest:
    - name: https://github.com/synergy/synergy.git
    - rev: v{{ version }}-stable
    - user: colby
    - target: {{ syn_dir }}
  cmd.run:
    - name: './hm.sh conf -g 1 && ./hm.sh build && ./hm.sh package deb'
    - user: colby
    - shell: /bin/sh
    - cwd: {{ syn_dir }}
    - require:
      - pkg: build-essential
      - pkg: synergy
    - unless:
      - synergyc --version | grep {{ version }}

synergy-install:
  cmd.run:
    - name: 'dpkg -i {{ syn_dir }}/bin/synergy*.deb'
    - onchanges:
      - cmd: synergy
