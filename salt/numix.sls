numix:
  pkgrepo.managed:
    - name: numix-daily
    - ppa: numix/ppa
  pkg.installed:
    - pkgs:
      - numix-gtk-theme
      - numix-icon-theme
      - numix-icon-theme-bevel
      - numix-icon-theme-circle
      - numix-plymouth-theme
