include:
  - .various

{% set version = '1.5.3' %}
{% set ninja_dir = '/home/colby/Software/ninja' %}
ninja-build:
  git.latest:
    - name: https://github.com/martine/ninja.git
    - rev: v{{ version }}
    - user: colby
    - target: {{ ninja_dir }}
  cmd.run:
    - name: python configure.py --bootstrap && ninja
    - user: colby
    - shell: /bin/sh
    - cwd: {{ ninja_dir }}
    - onchanges:
      - git: ninja-build
    - require:
      - pkg: build-essential

ninja-copy:
  cmd.run:
    - name: cp {{ ninja_dir }}/ninja /usr/local/bin/ninja
    - onchanged:
      - cmd: ninja-build

