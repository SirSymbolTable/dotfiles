{% set version = '0.8.5' %}
{% set packer_dir = '/home/colby/Software/packer' %}
packer:
  file.managed:
    - name: {{ packer_dir }}/packer.zip
    - source: https://dl.bintray.com/mitchellh/packer/packer_{{ version }}_linux_amd64.zip
    - source_hash: md5=21d75c05e692e53114fccc3639942a74
    - makedirs: yes
  cmd.run:
    - name: unzip -d /usr/local/bin {{ packer_dir }}/packer.zip
    - onchanges:
      - file: packer
