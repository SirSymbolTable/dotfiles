
{% for pkg in [
  'git',
  'vim',
  'meld',
  'python-pip',
  'python3-pip',
  'build-essential',
  'vagrant',
]%}
{{ pkg }}:
  pkg.installed:
    - refresh: no
{% endfor %}

peru:
  pip.installed:
    - bin_env: /usr/bin/pip3
    - require:
      - pkg: python3-pip

{% for pkg in ['virtualenv', 'ipython', 'ptpython'] %}
{{ pkg }}:
  pip.installed:
    - require:
      - pkg: python-pip
{% endfor %}

