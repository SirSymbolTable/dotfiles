{% set version = '1.7.1' %}
docker:
  file.managed:
    - name: /etc/apt/sources.list.d/docker.list
    - contents: |
        deb https://get.docker.com/ubuntu docker main
  pkg.installed:
    - name: lxc-docker
    - version: {{ version }}
    - require:
      - file: docker
