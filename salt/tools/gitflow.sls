include:
  - .various
{% set version = '1.8.0' %}
{% set git_flow_dir = '/home/colby/Software/git-flow' %}

git-flow:
  git.latest:
    - name: https://github.com/petervanderdoes/gitflow.git
    - rev: {{ version }}
    - user: colby
    - target: {{ git_flow_dir }}
  cmd.run:
    - name: make install
    - cwd: {{ git_flow_dir }}
    - require:
      - git: git-flow
    - unless:
      - git flow version | grep {{ version }}
