{% set version = '3.5' %}
{% set patch = '0' %}
{% set full_version = version + '.' + patch %}
{% set cmake_dir = '/home/colby/Software/cmake' %}
cmake-installer:
  file.managed:
    - name: {{ cmake_dir }}/cmake-installer-{{ full_version }}.sh
    - makedirs: true
    - source: https://www.cmake.org/files/v{{ version }}/cmake-{{ full_version }}-Linux-x86_64.sh
    - source_hash: md5=045d11334c4dbf4c974e341d1d34ca9f

install-cmake:
  cmd.run:
    - name: sh {{ cmake_dir }}/cmake-installer-{{ full_version }}.sh --prefix=/usr/local --exclude-subdir
    - onchanges:
      - file: cmake-installer
