fish:
  pkgrepo.managed:
    - ppa: fish-shell/release-2
  pkg.latest:
    - refresh: true
    - require: [pkgrepo: fish]
