{% set version = '2.10.11' %}

pidgin:
  pkgrepo.managed:
    - name: 'Pidgin PPA'
    - ppa: pidgin-developers/ppa
  pkg.latest:
    - pkgs:
      - pidgin
      - libpurple0
      - pidgin-otr
    - require:
      - pkgrepo: pidgin
