include:
  - core-utils

{% set codename = salt['cmd.run']('lsb_release -cs') %}
{% set version = '3.8' %}

llvm-repo:
  file.managed:
    - name: /etc/apt/sources.list.d/llvm-upstream.list
    - contents: |
        deb http://llvm.org/apt/{{ codename }}/ llvm-toolchain-{{ codename }} main
        deb-src http://llvm.org/apt/{{ codename }}/ llvm-toolchain-{{ codename }} main

add-llvm-key:
  cmd.run:
    - name: 'wget -O- http://llvm.org/apt/llvm-snapshot.gpg.key | sudo apt-key add -'
    - onchanges:
      - file: llvm-repo
    - require:
      - pkg: wget

llvm-packages:
  pkg.installed:
    - pkgs:
      - clang-{{ version }}
      - lldb-{{ version }}
      - python-clang-{{ version }}
      - libclang-common-{{ version }}-dev
      - libclang-{{ version }}-dev
      - clang-modernize-{{ version }}
      - clang-format-{{ version }}

{% for bin in ['clang', 'clang++', 'clang-format']%}
clang-symlink-{{ bin }}:
  file.symlink:
    - name: /usr/local/bin/{{ bin }}
    - target: /usr/bin/{{ bin }}-{{ version }}
{% endfor %}
