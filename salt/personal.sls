include:
  - numix
  - pithos

personal-packages:
  pkg.installed:
    - pkgs:
      - xchat
      - gnome-system-monitor
      - indicator-multiload
      - unity-tweak-tool
      - synaptic
      - compizconfig-settings-manager
      - compiz-plugins
      - compiz-plugins-extra
      - gdebi
      - tig

