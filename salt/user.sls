include:
  - .tools.fish

colby:
  user.present:
    - fullname: Colby
    - shell: /usr/bin/fish
    - home: /home/colby
    - optional_groups:
      - wheel
      - sudo

