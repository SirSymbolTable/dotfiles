include:
  - user
  - tools

{% set config = {
  'diff.tool': 'meld',
  'core.editor': 'vim',
  'color.branch': 'auto',
  'color.diff': 'auto',
  'color.interactive': 'auto',
  'color.status': 'auto',
  'push.default': 'simple',
  'user.name': 'Colby Pike',
}%}
{% for key, value in config.iteritems() %}
git-config-{{ key }}:
  git.config:
    - name: {{ key }}
    - value: {{ value }}
    - is_global: yes
    - user: colby
    - require:
      - user: colby
{% endfor %}
