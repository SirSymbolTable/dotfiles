[unityshell]
s0_panel_opacity = 0.310000
s0_panel_opacity_maximized_toggle = true

[screenshot]
s0_selection_outline_color = #2f2f4f9f
s0_selection_fill_color = #2f2f4f4f

[firepaint]
s0_fire_color = #ff3305ff

[cubeaddon]
s0_ground_color1 = #b3b3b3cc
s0_ground_color2 = #b3b3b300

[gnomecompat]
s0_command_window_screenshot = gnome-screenshot -w
s0_command_terminal = x-terminal-emulator
s0_run_command_terminal_key = <Control><Alt>t

[resizeinfo]
s0_gradient_1 = #cccce6cc
s0_gradient_2 = #f3f3f3cc
s0_gradient_3 = #d9d9d9cc
s0_outline_color = #e6e6e6ff

[shift]
s0_ground_color1 = #b3b3b3cc
s0_ground_color2 = #b3b3b300

[core]
s0_active_plugins = core;composite;opengl;copytex;compiztoolbox;decor;grid;imgpng;mousepoll;place;unitymtgrabhandles;thumbnail;vpswitch;session;wall;resize;move;regex;wobbly;animation;workarounds;expo;ezoom;fade;scale;scalefilter;unityshell;
s0_outputs = 1920x1080+0+0;1920x1080+1920+0;
s0_autoraise = false
s0_autoraise_delay = 500
s0_show_desktop_key = <Control><Super>d
s0_hsize = 2
s0_vsize = 2

[animation]
s0_open_effects = animation:Glide 1;animation:Glide 2;animation:Glide 2;
s0_open_durations = 400;120;120;
s0_close_effects = animation:Glide 1;animation:Glide 2;animation:Glide 2;
s0_close_durations = 400;120;120;
s0_minimize_effects = animation:Sidekick;
s0_minimize_durations = 600;
s0_unminimize_effects = animation:Sidekick;
s0_glide1_away_angle = 35.000000
s0_sidekick_num_rotations = 0.100000

[decor]
s0_active_shadow_color = #00000080
s0_inactive_shadow_color = #000000ff

[freewins]
s0_snap_threshold = 50
s0_do_shape_input = false
s0_immediate_moves = false
s0_circle_color = #54befb80
s0_line_color = #1800ffff
s0_cross_line_color = #1800ffff

[scale]
s0_overlay_icon = 1

[expo]
s0_multioutput_mode = 0
s0_ground_color1 = #b3b3b3cc
s0_ground_color2 = #b3b3b300

[thumbnail]
s0_thumb_size = 650
s0_fade_speed = 0.100000
s0_border = 32
s0_thumb_color = #0000007f
s0_mipmap = true
s0_current_viewport = false
s0_font_background_color = #0000007f

[ezoom]
s0_zoom_box_outline_color = #2f2f4f9f
s0_zoom_box_fill_color = #2f2f2f4f

[put]
s0_put_left_key = <Control><Alt>KP_Left
s0_put_right_key = <Control><Alt>KP_Right
s0_put_top_key = <Control><Alt>KP_Up
s0_put_bottom_key = <Control><Alt>KP_Down
s0_put_topleft_key = <Control><Alt>KP_Home
s0_put_topright_key = <Control><Alt>KP_Prior
s0_put_bottomleft_key = <Control><Alt>KP_End
s0_put_bottomright_key = <Control><Alt>KP_Next

[showmouse]
s0_color = #ffdf3fff

[shelf]
s0_animtime = 1

[staticswitcher]
s0_next_key = Disabled
s0_prev_key = Disabled

[wall]
s0_preview_scale = 400
s0_border_width = 27
s0_thumb_highlight_gradient_shadow_color = #dfdfdfff
s0_arrow_base_color = #e6e6e6d9
s0_arrow_shadow_color = #dcdcdcd9
s0_edgeflip_move = true

