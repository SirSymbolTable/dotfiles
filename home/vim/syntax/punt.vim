" Vim syntax file for the Punt markup language

if exists("b:current_syntax")
  finish
endif

" syn keyword directives include
syn match classes ",\@1<=\(\_s|\w\)*"
" syn match tagName "\(\w\|-\)\+\_s*=\=\((\|{\|\[\)\@2="
" syn match idName "(\@1<=\_s*\w\+\_s*\()\|,\)\@1="
syn match attrval "\(\w:\_s*\)\@<=[^;}]*\ze\(;\|$\|}\)"
syn match attrname "\(\w\|-\|?\)*\_s*:\@1="
"syn match Comment "#.*"
syn match Comment "//.*"

syn keyword puntConditional       if else switch
syn keyword puntRepeat            while for do in
syn keyword puntBranch            break continue
syn keyword puntOperator          new delete instanceof typeof
syn keyword puntJsType            Array Boolean Date Function Number Object String RegExp
syn keyword puntType              action alias bool color date double enumeration font int list point real rect size string time url variant vector3d
syn keyword puntStatement         return with
syn keyword puntBoolean           true false
syn keyword puntNull              null undefined
syn keyword puntIdentifier        arguments this var
syn keyword puntLabel             case default
syn keyword puntException         try catch finally throw
syn keyword puntMessage           alert confirm prompt status
syn keyword puntGlobal            self
syn keyword puntDeclaration       property signal
syn keyword puntReserved          abstract boolean byte char class const debugger enum export extends final float goto implements import bimport emport interface long native package pragma private protected public short static super synchronized throws transient volatile


if get(g:, 'punt_fold', 0)
  syn match   puntFunction      "\<function\>"
  syn region  puntFunctionFold  start="\<function\>.*[^};]$" end="^\z1}.*$" transparent fold keepend

  syn sync match puntSync  grouphere puntFunctionFold "\<function\>"
  syn sync match puntSync  grouphere NONE "^}"

  setlocal foldmethod=syntax
  setlocal foldtext=getline(v:foldstart)
else
  syn keyword puntFunction function
  syn match   puntBraces   "[{}\[\]]"
  syn match   puntParens   "[()]"
endif


syn match   puntSpecial           "\\\d\d\d\|\\."
syn region  puntStringD           start=+"+  skip=+\\\\\|\\"\|\\$+  end=+"\|$+  contains=puntSpecial,@htmlPreproc
syn region  puntStringS           start=+'+  skip=+\\\\\|\\'\|\\$+  end=+'\|$+  contains=puntSpecial,@htmlPreproc

syn keyword keywords property var this behaviors return observe listen on
syn match keywords "!!\_s\+\zs\<\(define-element\|bind-area\)\>"
"             |   <  tag name  >    <    is-identity  >    <ending brace>
syn match tag "\zs\(\w\|[:-]\)\+\ze\(=\(\w\|[:-]\)*\)\?\_s*\((\|{\|\[\|<\)\@2="
syn match idName "<\zs\(\w\|[:-]\)\+\ze>"
syn match String "\(\[\[\)\@3<=\(\_s\|.\)\{-}\(\]\]\)\@3="
syn match propertyDecl "property\(\_s\|\w\)\+\zs\w+\ze\_s*:"


let b:current_syntax = "punt"

hi def link directives PreProc
hi def link keywords PreProc
hi def link tag Type
hi def link propertyDecl Type
hi def link idName Statement
hi def link classes Type
hi def link attrname Statement
hi def link attrval Constant

hi def link puntComment           Comment
hi def link puntLineComment       Comment
hi def link puntCommentTodo       Todo
hi def link puntSpecial           Special
hi def link puntStringS           String
hi def link puntStringD           String
hi def link puntCharacter         Character
hi def link puntNumber            Number
hi def link puntConditional       Conditional
hi def link puntRepeat            Repeat
hi def link puntBranch            Conditional
hi def link puntOperator          Operator
hi def link puntJsType            Type
hi def link puntType              Type
hi def link puntObjectLiteralType Type
hi def link puntStatement         Statement
hi def link puntFunction          Function
hi def link puntBraces            Function
hi def link puntError             Error
hi def link puntNull              Keyword
hi def link puntBoolean           Boolean
hi def link puntRegexpString      String

hi def link puntIdentifier        Identifier
hi def link puntLabel             Label
hi def link puntException         Exception
hi def link puntMessage           Keyword
hi def link puntGlobal            Keyword
hi def link puntReserved          Keyword
hi def link puntDebug             Debug
hi def link puntConstant          Label
hi def link puntNonBindingColon   NONE
hi def link puntBindingProperty   Label
hi def link puntDeclaration       Function

