message(STATUS "Executing peru to synchronize repositories...")

find_program(peru peru)
if(NOT peru)
    message(FATAL_ERROR "Cannot proceed without Peru! Please install the Peru synchronization tool")
endif()

execute_process(
    COMMAND "${peru}" sync -v
    WORKING_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}"
    RESULT_VARIABLE res
    )

if(res)
    message(FATAL_ERROR "Peru failed! Please check the output above and fix this error!")
endif()

message(STATUS "Done")

message(STATUS "Downloading upstream libclang")

if(WIN32)
    message(WARNING "We're not sure how to get the libclang libraries for Windows. Please do it manually and update prepare.cmake")
endif()

get_filename_component(ycm_base "${CMAKE_CURRENT_LIST_DIR}/YouCompleteMe" ABSOLUTE)
get_filename_component(ycm_build "${ycm_base}/build" ABSOLUTE)

file(MAKE_DIRECTORY "${ycm_build}")

execute_process(
    COMMAND "${CMAKE_COMMAND}" -D USE_SYSTEM_LIBCLANG=YES -G Ninja "${ycm_base}/third_party/ycmd/cpp"
    RESULT_VARIABLE res
    WORKING_DIRECTORY "${ycm_build}"
    )

if(res)
    message(FATAL_ERROR "Failed to configure YouCompleteMe")
endif()

execute_process(
    COMMAND "${CMAKE_COMMAND}" --build "${ycm_build}"
    RESULT_VARIABLE res
    )
if(res)
    message(FATAL_ERROR "Failed to build YouCompleteMe")
endif()
