import os
import ycm_core

default_flags = [
    '-Wall',
    '-Wextra',
    '-std=c++14',
    '-x',
    '-c++',
]

SOURCE_EXTENSIONS = ['.cpp', '.cxx', '.cxx', '.c', '.m', '.mm']


_found_db = None

for check in [
    '../build/compile_commands.json',
    'build/compile_commands.json',
    'compile_commands.json',]:
    if os.path.exists(check):
        _found_db = ycm_core.CompilationDatabase(os.path.dirname(check))


def load_db_entry(filepath):
    global _found_db
    if isinstance(_found_db, ycm_core.CompilationDatabase):
        return _found_db.GetCompilationInfoForFile(filepath)
    abspath = os.path.abspath(filepath)
    # Stop climbing if we are in the user's home directory
    if abspath == os.path.expanduser('~'):
        return None
    dirname = os.path.dirname(abspath)
    entries = os.listdir(dirname)
    if 'compile_commands.json' in entries:
        _found_db = ycm_core.CompilationDatabase(dirname)
        return load_db_entry(filepath)
    else:
        for entry in entries:
            subpath = os.path.join(dirname, entry)
            if os.path.isdir(subpath) and 'compile_commands.json' in os.listdir(subpath):
                _found_db = ycm_core.CompilationDatabase(subpath)
                return load_db_entry(filepath)
        return load_db_entry(dirname)

def is_header(filepath):
    return os.path.splitext(filepath)[1] in ('.h', '.hxx', '.tcc', '.hh', '.hpp')


def find_file_compile_command(filepath):
    if is_header(filepath):
        for ext in SOURCE_EXTENSIONS:
            new_path = os.path.splitext(filepath)[0] + ext
            if os.path.exists(new_path):
                res = find_file_compile_command(new_path)
                if res is not None:
                    return res
        return None
    entry = load_db_entry(filepath)
    if entry is None:
        return None
    return MakeRelativePathsInFlagsAbsolute(entry.compiler_flags_, entry.compiler_working_dir_)


def MakeRelativePathsInFlagsAbsolute( flags, working_directory ):
  if not working_directory:
    return list( flags )
  new_flags = []
  make_next_absolute = False
  path_flags = [ '-isystem', '-I', '-iquote', '--sysroot=' ]
  for flag in flags:
    new_flag = flag

    if make_next_absolute:
      make_next_absolute = False
      if not flag.startswith( '/' ):
        new_flag = os.path.join( working_directory, flag )

    for path_flag in path_flags:
      if flag == path_flag:
        make_next_absolute = True
        break

      if flag.startswith( path_flag ):
        path = flag[ len( path_flag ): ]
        new_flag = path_flag + os.path.join( working_directory, path )
        break

    if new_flag:
      new_flags.append( new_flag )
  return new_flags


def FlagsForFile( filename, **kwargs ):
    flags = find_file_compile_command(filename)
    return {
        'flags': flags or default_flags,
        'do_cache': flags is not None
    }
